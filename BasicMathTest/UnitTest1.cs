﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicMath;
namespace BasicMathTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            BasicMaths bm = new BasicMaths();
            double res = bm.Add(10, 10);
            Assert.AreEqual(res, 20);
        }
    }
}
